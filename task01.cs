using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task01
{
    class task01
    {
        static void meth( )
        {
            Console.WriteLine("This is a method.");
            Console.WriteLine("The second line is now printed to the screen.");
        }
        static void Main(string[] args)
        {
            meth();
        }
    }
}